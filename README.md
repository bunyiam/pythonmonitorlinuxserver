# ระบบแจ้งเตือนการใช้ทรัพยากร Linux

## Install psutil , platform , datetime , requests On Ubuntu
sudo apt-get update

sudo apt-get install python3-psutil

sudo apt-get install python-psutil

sudo apt-get install python3-pip

sudo pip3 install platform

sudo pip install platform

sudo apt-get install python3-pip

sudo pip3 install datetime

sudo pip install datetime

sudo apt-get install python3-pip

sudo pip3 install requests

sudo pip install requests


# Set ตั้งเวลาด้วย Crontab

crontab -e

## ให้แจ้งเตือนผ่าน Line Notify ทุก เวลา 10.01 น. และ 14.01 น. ของทุกวัน

1 10 * * * /usr/bin/python3 /root/pythonmonitorlinuxserver.py > /dev/null

1 14 * * * /usr/bin/python3 /root/pythonmonitorlinuxserver.py > /dev/null

## Run

เพิ่ม สิทธิ์ การทำงานฯ ใช้คำสั่ง chmod +x pythonmonitorlinuxserver.py , chmod +x r.text
